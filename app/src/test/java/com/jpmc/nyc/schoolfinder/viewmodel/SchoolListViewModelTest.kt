package com.jpmc.nyc.schoolfinder.viewmodel

import android.app.Application
import com.jpmc.nyc.schoolfinder.api.NetworkResult
import com.jpmc.nyc.schoolfinder.api.SchoolsDirectoryResponse
import com.jpmc.nyc.schoolfinder.model.School
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
class SchoolListViewModelTest {

    private val dispatcher = TestCoroutineDispatcher()
    private val schoolsDirectoryResponse = mock<SchoolsDirectoryResponse>()
    private val schoolsDirectoryChannel = Channel<NetworkResult<List<School>>>()
    private val schoolsDirectoryFlow = schoolsDirectoryChannel.consumeAsFlow()
    private val application = mock<Application>()
    private val vm = SchoolListViewModel(schoolsDirectoryResponse, application)

    @Before
    fun before() {
        Dispatchers.setMain(dispatcher)
        doReturn(schoolsDirectoryFlow).whenever(schoolsDirectoryResponse.fetchSchools())
        vm.isDataLoadingLD.observeForever { }
        vm.schoolDirectoryLD.observeForever { }

    }

    @Test
    fun `test isDataLoadingLD value before and after data fetch`() {
        assertEquals(true, vm.isDataLoadingLD.value)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}
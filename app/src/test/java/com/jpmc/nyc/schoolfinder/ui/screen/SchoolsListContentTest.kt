package com.jpmc.nyc.schoolfinder.ui.screen

import android.os.Build
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jpmc.nyc.schoolfinder.model.School
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config


@RunWith(AndroidJUnit4::class)
@Config(minSdk = Build.VERSION_CODES.P)
class SchoolsListContentTest {

    @get:Rule
    val composeRule = createComposeRule()

    val schoolList = mutableListOf<School>().apply {
        this.add(School("School 1", "school1", "718-542-0740", null, null, null,null, null, null))
        this.add(School("School 2", "school2",null, "sburns@schools.nyc.gov", null, null,null, null, null))
        this.add(School("School 3", "school2",null, null, null, null,null, null, null))
        this.add(School("School 4 very very very very very very very very long name", "school2","718-542-0740", "sburns@schools.nyc.gov", null, null,null, null, null))
    }

    @ExperimentalFoundationApi
    @Test
    fun `test screen header`() {
        composeRule.setContent {
            SchoolsListContent(schoolList = schoolList, onListItemClicked = {} )
        }

        composeRule.onNodeWithTag("screen_header")
            .assertTextEquals("NYC High School Director")
    }
}
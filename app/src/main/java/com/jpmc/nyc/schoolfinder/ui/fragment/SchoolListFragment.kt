package com.jpmc.nyc.schoolfinder.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.jpmc.nyc.schoolfinder.R
import com.jpmc.nyc.schoolfinder.api.NetworkResult
import com.jpmc.nyc.schoolfinder.databinding.FragmentSchoolListBinding
import com.jpmc.nyc.schoolfinder.ui.screen.SchoolsListContent
import com.jpmc.nyc.schoolfinder.viewmodel.SchoolListViewModel

class SchoolListFragment : BaseFragment() {

    private lateinit var  binding : FragmentSchoolListBinding

    private lateinit var schoolListViewModel : SchoolListViewModel


    override fun onAttach(context: Context) {
        super.onAttach(context)
        schoolListViewModel = ViewModelProvider(requireActivity()).get(SchoolListViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentSchoolListBinding.inflate(inflater, container, false)
        binding.schoolListVM = schoolListViewModel
        binding.lifecycleOwner = this
        return binding.root

    }

    @OptIn(ExperimentalFoundationApi::class)
    override fun onResume() {
        super.onResume()
        schoolListViewModel.isDataLoadingLD.value = true
        observeSchoolData()
    }

    override fun onPause() {
        super.onPause()
        schoolListViewModel.schoolDirectoryLD.removeObserver { this }
    }

    override fun retryAPICall() {
        schoolListViewModel.fetchSchools()
    }

    @ExperimentalFoundationApi
    private fun observeSchoolData() {
        schoolListViewModel.schoolDirectoryLD.observe(viewLifecycleOwner) { response ->
            when (response) {
                is NetworkResult.Success -> {
                    // bind data to the view
                    val schools = response.data
                    binding.schoolListComposeScreen.setContent {
                        schools?.let {
                            SchoolsListContent(it) { school ->
                                val bundle = Bundle()
                                bundle.putSerializable(
                                    SchoolDetailFragment.ARG_ITEM,
                                    school
                                )
                                this.findNavController().navigate(R.id.show_item_detail, bundle)
                            }
                        }
                    }
                    schoolListViewModel.isDataLoadingLD.value = false
                }
                is NetworkResult.Error -> {
                    schoolListViewModel.isDataLoadingLD.value = false
                    showAlertDialog()
                }
                is NetworkResult.Loading -> {
                    schoolListViewModel.isDataLoadingLD.value = true
                }
            }
        }
    }
}
package com.jpmc.nyc.schoolfinder.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class School(
    @SerializedName("school_name")
    val schoolName: String,
    val dbn: String,
    @SerializedName("phone_number")
    val phone: String?,
    @SerializedName("school_email")
    val email: String?,
    val website: String?,
    @SerializedName("primary_address_line_1")
    val address: String?,
    val city: String?,
    val zip: String?,
    @SerializedName("state_code")
    val state: String?
    ) : Serializable

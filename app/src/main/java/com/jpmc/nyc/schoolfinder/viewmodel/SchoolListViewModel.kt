package com.jpmc.nyc.schoolfinder.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.jpmc.nyc.schoolfinder.api.NetworkResult
import com.jpmc.nyc.schoolfinder.api.SchoolsDirectoryResponse
import com.jpmc.nyc.schoolfinder.model.School
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolListViewModel @Inject constructor
    (
    private val schoolsDirectoryResponse: SchoolsDirectoryResponse,
    application: Application
) : AndroidViewModel(application) {

    val isDataLoadingLD = MutableLiveData(true)
    val schoolDirectoryLD: MutableLiveData<NetworkResult<List<School>>> = MutableLiveData()

    init {
        fetchSchools()
    }

    fun fetchSchools() = viewModelScope.launch {
        isDataLoadingLD.postValue(true)
        schoolsDirectoryResponse.fetchSchools().collect { values ->
            schoolDirectoryLD.value = values
            isDataLoadingLD.postValue(false)
        }
    }
}
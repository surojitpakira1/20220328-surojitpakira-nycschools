package com.jpmc.nyc.schoolfinder.api

import javax.inject.Inject

class RemoteDataSource @Inject constructor(private val nycSchoolApi : NYCSchoolApi) {

    suspend fun fetchSchools() = nycSchoolApi.fetchSchools()

    suspend fun fetchSATResult(dbn: String) = nycSchoolApi.fetchSATResult(dbn)
}
package com.jpmc.nyc.schoolfinder.ui.fragment

import android.Manifest.permission.CALL_PHONE
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.jpmc.nyc.schoolfinder.api.NetworkResult
import com.jpmc.nyc.schoolfinder.databinding.FragmentSchoolDetailBinding
import com.jpmc.nyc.schoolfinder.model.SATResult
import com.jpmc.nyc.schoolfinder.model.School
import com.jpmc.nyc.schoolfinder.ui.screen.SchoolScoreContent
import com.jpmc.nyc.schoolfinder.viewmodel.SchoolDetailsViewModel


class SchoolDetailFragment : BaseFragment() {

    private var selectedSchool: School? = null

    private var toolbarLayout: CollapsingToolbarLayout? = null

    private  lateinit var binding : FragmentSchoolDetailBinding

    private lateinit var schoolDetailsViewModel: SchoolDetailsViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        schoolDetailsViewModel = ViewModelProvider(requireActivity()).get(SchoolDetailsViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let { it ->
            if (it.containsKey(ARG_ITEM)) {
                selectedSchool = it.get(ARG_ITEM) as School
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentSchoolDetailBinding.inflate(inflater, container, false)
        val rootView = binding.root

        toolbarLayout = binding.toolbarLayout
        updateContent()

        binding.schoolDetailsVM = schoolDetailsViewModel
        binding.lifecycleOwner = this

        binding.fab.setOnClickListener {
            selectedSchool?.phone?.let {
                if (ContextCompat.checkSelfPermission(requireContext(), CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    callPhone()
                } else {
                    ActivityCompat.requestPermissions(requireActivity(), arrayOf(CALL_PHONE), 1)
                }
            }
        }

        return rootView
    }

    override fun onResume() {
        super.onResume()
        schoolDetailsViewModel.isSATDataLoadingLD.postValue(true)
        selectedSchool?.let { school->
            schoolDetailsViewModel.init(school)
        }
        observeSATData()
    }

    override fun onPause() {
        super.onPause()
        schoolDetailsViewModel.satResultLD.removeObserver { this }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED) {
                    if ((ContextCompat.checkSelfPermission(requireActivity(), CALL_PHONE) === PackageManager.PERMISSION_GRANTED)) {
                        callPhone()
                    }
                } else {
                    Toast.makeText(requireContext(), "Permission Denied..", Toast.LENGTH_SHORT).show()
                }
                return
            }
        }
    }

    override fun retryAPICall() {
        schoolDetailsViewModel.fetchSATResult()
    }

    private fun updateContent() {
        toolbarLayout?.title = selectedSchool?.schoolName
    }

    private fun callPhone() {
        selectedSchool?.phone?.let { phone ->
            val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$phone"))
            startActivity(intent)
        }
    }
    private fun observeSATData() {
        schoolDetailsViewModel.satResultLD.observe(viewLifecycleOwner) { response ->
            when (response) {
                is NetworkResult.Success -> {
                    // bind data to the view
                    binding.schoolScoreComposeScreen.setContent {
                        val satResult = response.data
                        satResult?.let { res ->
                            var satRes : SATResult? = null
                            if(!res.isNullOrEmpty()){
                                res[0].also { satRes = it }
                            }
                            selectedSchool?.let {
                                SchoolScoreContent(school = it, satResult = satRes)
                            }

                        }
                    }
                    schoolDetailsViewModel.isSATDataLoadingLD.value = false

                }
                is NetworkResult.Error -> {
                    showAlertDialog()
                    schoolDetailsViewModel.isSATDataLoadingLD.value = false
                }
                is NetworkResult.Loading -> {
                    schoolDetailsViewModel.isSATDataLoadingLD.postValue(true)
                }
            }
        }
    }

    companion object {
        /**
         * The fragment argument representing the selected School that this fragment
         * represents.
         */
        const val ARG_ITEM = "selected_school"
    }
}
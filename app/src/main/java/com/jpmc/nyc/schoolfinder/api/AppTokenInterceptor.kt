package com.jpmc.nyc.schoolfinder.api

import com.jpmc.nyc.schoolfinder.api.EndPoints.Companion.APP_TOKEN
import okhttp3.Interceptor
import okhttp3.Response

class AppTokenInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val newRequest = chain.request().newBuilder()
            .addHeader("X-App-Token", APP_TOKEN)
            .addHeader("Accept", "application/json")
            .build()

        return chain.proceed(newRequest)
    }
}
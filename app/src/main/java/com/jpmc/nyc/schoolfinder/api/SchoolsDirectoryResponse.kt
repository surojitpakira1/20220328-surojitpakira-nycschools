package com.jpmc.nyc.schoolfinder.api

import com.jpmc.nyc.schoolfinder.model.SATResult
import com.jpmc.nyc.schoolfinder.model.School
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

@ActivityRetainedScoped
class SchoolsDirectoryResponse @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : BaseApiResponse() {

    fun fetchSchools(): Flow<NetworkResult<List<School>>> {
        return flow {
            emit(safeApiCall { remoteDataSource.fetchSchools() })
        }.flowOn(Dispatchers.IO)
    }

    fun fetchSATResults(dbn: String): Flow<NetworkResult<List<SATResult>>> {
        return flow {
            emit(safeApiCall { remoteDataSource.fetchSATResult(dbn) })
        }.flowOn(Dispatchers.IO)
    }
}
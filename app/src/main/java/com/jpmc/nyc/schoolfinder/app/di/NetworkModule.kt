package com.jpmc.nyc.schoolfinder.app.di

import com.jpmc.nyc.schoolfinder.api.AppTokenInterceptor
import com.jpmc.nyc.schoolfinder.api.EndPoints.Companion.BASE_URL
import com.jpmc.nyc.schoolfinder.api.NYCSchoolApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Singleton
    @Provides
    fun provideAppTokenInterceptor(): AppTokenInterceptor {
        return AppTokenInterceptor()
    }

    @Singleton
    @Provides
    fun provideHttpClient(appTokenInterceptor: AppTokenInterceptor): OkHttpClient {

        val builder = OkHttpClient.Builder()
        // We add the interceptor to OkHttpClient.
        // It will add app token headers to every call we make.
        builder.readTimeout(15, TimeUnit.SECONDS)
            .connectTimeout(15, TimeUnit.SECONDS)
            .interceptors().add(appTokenInterceptor)

        return builder.build()
    }

    @Singleton
    @Provides
    fun provideConverterFactory(): GsonConverterFactory =
        GsonConverterFactory.create()

    @Singleton
    @Provides
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(gsonConverterFactory)
            .build()
    }
    @Singleton
    @Provides
    fun provideNYCSchoolsService(retrofit: Retrofit): NYCSchoolApi =
        retrofit.create(NYCSchoolApi::class.java)
}
package com.jpmc.nyc.schoolfinder.ui.screen

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Call
import androidx.compose.material.icons.filled.Email
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.jpmc.nyc.schoolfinder.R
import com.jpmc.nyc.schoolfinder.model.SATResult
import com.jpmc.nyc.schoolfinder.model.School


@Preview(showBackground = true)
@Composable
fun SchoolScoreContentPreview() {
    val school = School("School name", "school2","718-542-0740", "sburns@schools.nyc.gov", null, "2865 West 19th Street","Brooklyn", "11224", "NY")
    val satResult = SATResult("School name", "school2","50", "70","90","80")
    SchoolScoreContent(school,satResult)
}

@Composable
fun SchoolScoreContent(school: School, satResult: SATResult?) {
    Row (modifier = Modifier
        .fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.CenterVertically),
        ) {
            SchoolDetailsCard(school = school)
            SchoolSATScoreCard(satResult = satResult)
        }
    }
}


@Composable
private fun SchoolDetailsCard(school: School) {
    Card(
        modifier = Modifier
            .padding(15.dp)
            .fillMaxWidth(),
        elevation = 2.dp,
        backgroundColor = Color.White,
        border = BorderStroke(1.dp,Color(129,184,230)),
        shape = RoundedCornerShape(corner = CornerSize(16.dp)),
    ) {
        Row (modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp), verticalAlignment = Alignment.CenterVertically) {
            Column(
                modifier = Modifier
                    .padding(10.dp)
                    .fillMaxWidth()
                    .align(Alignment.CenterVertically),
            ) {
                HeaderRow(stringResource(id = R.string.contact_details))
                DetailsRow(school.address)
                SecondaryAddressRow(school)
                DetailsRow(school.website, TextDecoration.Underline)
            }
        }

    }
}

@Composable
private fun SchoolSATScoreCard(satResult: SATResult?) {
    Card(
        modifier = Modifier
            .padding(15.dp)
            .fillMaxWidth(),
        elevation = 2.dp,
        backgroundColor = Color.White,
        border = BorderStroke(1.dp,Color(129,184,230)),
        shape = RoundedCornerShape(corner = CornerSize(16.dp)),
    ) {
        Row (modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp), verticalAlignment = Alignment.CenterVertically) {
            if(satResult == null) {
                Column(
                    modifier = Modifier
                        .padding(10.dp)
                        .fillMaxWidth()
                        .align(Alignment.CenterVertically),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = stringResource(id = R.string.no_sat_result),
                        style = typography.subtitle1,
                        fontSize = 17.sp,
                        color = Color.DarkGray,
                        fontWeight = FontWeight.SemiBold,
                        modifier = Modifier.padding(3.dp)
                    )
                }
            } else {
                Column(
                    modifier = Modifier
                        .padding(10.dp)
                        .fillMaxWidth()
                        .align(Alignment.CenterVertically),
                ) {
                    HeaderRow(stringResource(id = R.string.sat_score))
                    ScoreDetailsRow(label = stringResource(id = R.string.label_total), satResult.totalCount)
                    ScoreDetailsRow(label = stringResource(id = R.string.label_reading_score), satResult.readingAvgScore)
                    ScoreDetailsRow(label = stringResource(id = R.string.label_writing_score), satResult?.writingAvgScore)
                    ScoreDetailsRow(label = stringResource(id = R.string.label_math_score), satResult?.mathAvgScore)
                }
            }
        }
    }
}

@Composable
private fun ScoreDetailsRow(label: String, value: String?) {
    value?.let {
        Row (modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically) {
            Text(text = label , style = typography.body2, modifier = Modifier.padding(5.dp))
            Text(text = it , style = typography.body2, modifier = Modifier.padding(5.dp))
        }
    }
}

@Composable
private fun HeaderRow(label: String) {
    Text(text = label,
        style = typography.subtitle1,
        fontSize = 17.sp,
        color = Color.DarkGray,
        fontWeight = FontWeight.SemiBold,
        textDecoration = TextDecoration.Underline,
        modifier = Modifier.padding(3.dp)
    )
}

@Composable
private fun DetailsRow(value: String?, textDecoration: TextDecoration? = null) {
    value?.let {
        Row (modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically) {
            Text(text = it ,
                style = typography.body2,
                textDecoration = textDecoration,
                modifier = Modifier.padding(5.dp))
        }
    }
}

@Composable
private fun SecondaryAddressRow(school: School) {
    Row (modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically) {

        school.city?.let {
            AddressText(it)
        }

        school.state?.let {
            AddressText(it)
        }

        school.zip?.let {
            AddressText(it)
        }
    }
}

@Composable
private fun AddressText(value: String) {
    Text(text = value , style = typography.body2, modifier = Modifier.padding(5.dp))
}

@Composable
private fun IconComposable(imageVector: ImageVector, tint: Color?) {
    if (tint != null) {
        Icon(
            imageVector = imageVector,
            contentDescription = stringResource(id = R.string.contact_details),
            tint = tint,
            modifier = Modifier.padding(2.dp)
        )
    }
}

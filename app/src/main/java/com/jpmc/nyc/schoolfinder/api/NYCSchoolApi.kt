package com.jpmc.nyc.schoolfinder.api

import com.jpmc.nyc.schoolfinder.model.SATResult
import com.jpmc.nyc.schoolfinder.model.School
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NYCSchoolApi {

    @GET("s3k6-pzi2.json")
    suspend fun fetchSchools() : Response<List<School>>

    @GET("f9bf-2cp4.json")
    suspend fun fetchSATResult(@Query("dbn") dbn:String) : Response<List<SATResult>>
}
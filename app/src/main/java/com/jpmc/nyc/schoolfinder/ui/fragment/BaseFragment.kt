package com.jpmc.nyc.schoolfinder.ui.fragment

import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.jpmc.nyc.schoolfinder.R

abstract class BaseFragment : Fragment() {

    fun showAlertDialog() {
        val dialogBuilder = AlertDialog.Builder(requireContext())
        dialogBuilder.setMessage(getString(R.string.error_dialog_message))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.error_dialog_retry)) { _, _ ->
                retryAPICall()
            }
            .setNegativeButton(getString(R.string.error_dialog_cancel)) { _, _ ->
                requireActivity().finish()
            }

        val alert = dialogBuilder.create()
        alert.setTitle(getString(R.string.error_dialog_header))
        alert.show()
    }

    abstract fun retryAPICall()
}
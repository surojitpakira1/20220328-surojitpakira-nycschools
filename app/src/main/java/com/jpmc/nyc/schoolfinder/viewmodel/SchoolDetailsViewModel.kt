package com.jpmc.nyc.schoolfinder.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.jpmc.nyc.schoolfinder.api.NetworkResult
import com.jpmc.nyc.schoolfinder.api.SchoolsDirectoryResponse
import com.jpmc.nyc.schoolfinder.model.SATResult
import com.jpmc.nyc.schoolfinder.model.School
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolDetailsViewModel @Inject constructor
    (
    private val schoolsDirectoryResponse: SchoolsDirectoryResponse,
    application: Application
) : AndroidViewModel(application) {

    private val selectedSchoolLD = MutableLiveData<School>()
    val isSATDataLoadingLD = MutableLiveData(true)
    val satResultLD: MutableLiveData<NetworkResult<List<SATResult>>> = MutableLiveData()

    fun init(selectedSchool : School) {
        selectedSchoolLD.value = selectedSchool
        fetchSATResult()
    }

    fun fetchSATResult() = viewModelScope.launch {
        isSATDataLoadingLD.postValue(true)
        selectedSchoolLD.value?.let {
            schoolsDirectoryResponse.fetchSATResults(it.dbn).collect { values ->
                satResultLD.value = values
            }
        }
    }
}
package com.jpmc.nyc.schoolfinder.ui.screen

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Call
import androidx.compose.material.icons.filled.Email
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.jpmc.nyc.schoolfinder.R
import com.jpmc.nyc.schoolfinder.model.School

@ExperimentalFoundationApi
@Preview(showBackground = true)
@Composable
fun SchoolListContentPreview() {
    val schoolList = mutableListOf<School>().apply {
        this.add(School("School 1", "school1", "718-542-0740", null, null, null,null, null, null))
        this.add(School("School 2", "school2",null, "sburns@schools.nyc.gov", null, null,null, null, null))
        this.add(School("School 3", "school2",null, null, null, null,null, null, null))
        this.add(School("School 4 very very very very very very very very long name", "school2","718-542-0740", "sburns@schools.nyc.gov", null, null,null, null, null))
    }
    SchoolsListContent(schoolList) {}
}

@ExperimentalFoundationApi
@Composable
fun SchoolsListContent(
    schoolList : List<School>,
    onListItemClicked: (School) -> Unit){

    LazyColumn(
        modifier = Modifier.fillMaxWidth(),
        contentPadding = PaddingValues(horizontal = 4.dp, vertical = 10.dp)
    ) {
        stickyHeader {
            Header()
        }
        var index = 0
        items(schoolList) { school ->
            val backgroundColor = if(index % 2 == 0) {
                Color(129,184,230)
            } else {
                Color(202,228,250)
            }
            SchoolListItem(school, backgroundColor, onListItemClicked)
            index++
        }
    }
}

@Composable
private fun Header() {
    Text(
        text = stringResource(id = R.string.list_header),
        color = Color.DarkGray,
        style = typography.h6,
        modifier = Modifier
            .background(Color.White)
            .padding(horizontal = 8.dp, vertical = 10.dp)
            .fillMaxWidth()
            .testTag("screen_header")
    )
}

@Composable
private fun SchoolListItem(school: School, backgroundColor: Color, onListItemClicked: (School) -> Unit) {
    Card(
        modifier = Modifier
            .padding(horizontal = 5.dp, vertical = 5.dp)
            .fillMaxWidth()
            .clickable {  onListItemClicked(school) },
        elevation = 2.dp,
        backgroundColor = backgroundColor,
        shape = RoundedCornerShape(corner = CornerSize(16.dp)),
    ) {
        Row {
            Column(
                modifier = Modifier
                    .padding(15.dp)
                    .fillMaxWidth()
                    .align(Alignment.CenterVertically),
            ) {
                Text(text = school.schoolName, style = typography.subtitle1, maxLines = 1, overflow = TextOverflow.Ellipsis)
                Spacer(modifier = Modifier.padding(3.dp))
                if(school.email == null && school.phone == null) {
                    ContactRow(stringResource(id = R.string.view_details),null, null)
                } else {
                    school.email?.let { ContactRow(it, Icons.Default.Email, Color.Blue) }
                    school.phone?.let { ContactRow(it, Icons.Default.Call, Color.Green) }
                }
            }
        }
    }
}

@Composable
private fun ContactRow(contact: String, imageVector: ImageVector?, tint: Color?) {
    Row (modifier = Modifier
        .fillMaxWidth()
        .padding(vertical = 1.dp, horizontal = 10.dp), verticalAlignment = Alignment.CenterVertically) {
        imageVector?.let { IconComposable(it, tint)}
        Text(text = contact, style = typography.caption, modifier = Modifier.padding(horizontal = 5.dp))
    }
}

@Composable
private fun IconComposable(imageVector: ImageVector, tint: Color?) {
    if (tint != null) {
        Icon(
            imageVector = imageVector,
            contentDescription = stringResource(id = R.string.contact_details),
            tint = tint,
            modifier = Modifier.padding(2.dp)
        )
    }
}